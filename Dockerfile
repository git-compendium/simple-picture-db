FROM node:16-alpine
WORKDIR /src
COPY package*.json /src/
RUN npm i --production
COPY backend/ /src/backend/
COPY frontend/ /src/frontend/
CMD ["npm", "start"]
