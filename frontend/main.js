function uploadPic() {
  let formData = new FormData();
  const pic = document.querySelector('input[type="file"]').files[0];
  if (pic === undefined) {
    return;
  }
  formData.append('file', pic);
  fetch('/api/picture', {
      method: 'POST',
      body: formData
    })
    .then(data => {
      fetchPics();
    });
}

function fetchPics() {
  fetch('/api/pictures')
    .then(data => data.json())
    .then(pics => {
      const ul = document.querySelector('#pics');
      while (ul.firstChild) {
        ul.removeChild(ul.firstChild);
      }
      for (let pic of pics.data) {
        let li = document.createElement("li");
        let img = document.createElement("img");
        let span = document.createElement("span");
        img.src = `data:${pic.mime};base64,${btoa(String.fromCharCode.apply(null,pic.thumbnail.data))}`;
        let deleteButton = document.createElement("button");
        deleteButton.onclick = function () {
          fetch(`/api/picture/${pic.id}`, {
              method: 'DELETE'
            })
            .then(data => fetchPics());
        };
        deleteButton.innerHTML = 'Delete';
        let picDate = new Date(pic.date * 1000);
        span.innerHTML = picDate.toDateString();
        li.appendChild(img);
        li.appendChild(deleteButton);
        li.append(span);
        ul.appendChild(li);
      }
    });
}
fetchPics();

